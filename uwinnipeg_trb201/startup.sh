#!/bin/bash
## UWINNIPEG TRB3 startup script
## Thome Lindner, Sept 2017. Edited by Andrew Sikora, 2018
##Edited to account for trb201 instead of trb165
## Script is annotated to explain what is being done.
##
## To execute this script do
## source startup.sh


DAQ_TOOLS_PATH=~/trbsoft/daqtools
USER_DIR=/home/trb3/trbsoft/daqtools/users/uwinnipeg_trb201
TRB_WEB_DIR=$DAQ_TOOLS_PATH/web

export PATH=$PATH:$DAQ_TOOLS_PATH
export PATH=$PATH:$DAQ_TOOLS_PATH/tools
export PATH=$PATH:$USER_DIR
export PATH=$PATH:/home/tbr3_user/trbsoft/trb3/dabc/bin/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/tbr3_user/trbsoft/trb3/dabc/lib/

### TL
### This part restarts the trbnetd server;
### this server takes instructions from trbcmd executable and
### passes them actually to the TRB.
export TRB3_SERVER=trb201:26000
export TRBNETDPID=$(pgrep -f "trbnetd -i 201")

export DAQOPSERVER=localhost:201

echo "- trbnetd pid: $TRBNETDPID"
if [[ -z "$TRBNETDPID" ]] 
then
    ~/trbsoft/trbnettools/bin/trbnetd -i 201
else
    echo "trbnetd already running"
fi

#./check_ping.pl --reboot

### TL
### Reset the FPGAs on the TRB3 itself
echo "reset"
./trbreset_loop.pl

# sleep 1;


##################################################
## Set addresses
## TL
## this set somehow "maps trbnet-addresses to serial number + FPGA numbers"
##################################################
merge_serial_address.pl $DAQ_TOOLS_PATH/base/serials_trb3.db $USER_DIR/db/addresses_trb3.db

#added AS
#trbcmd reset


## TL
## Seems clear that these calls seem to set details about the TRB3
## gigabit communication.
## configgbe.db is supposed to define the network connection for the TRB3.
## in particular, since we use UDP packets, we need to hardcode the MAC
## address of the event builder PC; daq14 in our case.
echo "GbE settings"
loadregisterdb.pl $USER_DIR/db/register_configgbe.db
#loadregisterdb.pl db/register_configgbe_ip.db #daq14
loadregisterdb.pl $USER_DIR/db/register_configgbe_ip_uwinnipeg.db  # daq11

## TL
## With this setting we define which type of extension boards we
## have installed; GPIN and padiwa in our case.
## Details here are a little fuzzy.  Sort of described in
## section "Slow Control registers"
echo "TDC settings"
loadregisterdb.pl $USER_DIR/db/register_configtdc.db
echo "TDC settings end"


# set correct timeout: off for channel 0, 1, 2sec for 2
# TL: I don't know what this does...
# MT: this sets timeouts on the different TRBNet channels, so if an answer is missing
#      there will be an error code, instead of waiting forever. Has no effect on a perfectly working system....
trbcmd w 0xfffe 0xc5 0x50ff

#trbcmd w 0xc001 0xa101 0xffff0040

#reset trigger logic
#trbcmd w 0xffff 0x20 0x33

echo "pulser"
# TL Set the pulser frequency
# I can't find the right documentation on this register...
# pulser #0 to 10 kHz
trbcmd w 0xc001 0xa156 0x0000270f   
#trbcmd w 0xc001 0xa156 0x0000070f   
#trbcmd w 0xc001 0xa150 0x0022270f   


# TL No full description of this XML-DB... not sure what this does...
# Gives error message, so comment out
# MT: Don't do that. The Data Limit has to be set correctly! Otherwise you get random 
#     stops, if the data is by accident to large
cd ~/trbsoft/daqtools/xml-db
./put.pl Readout 0xfe4c SetMaxEventSize 500
cd $USER_DIR

# TL also can't find documentation on this register
# Gives error message, so comment out...
#trbcmd w 0xfe51 0xdf80 0xffffffff # enable monitor counters

#echo "trigger type"
# set trigger type to 0x1
#trbcmd setbit 0xc001 0xa155 0x10

echo "enable trigger channels"
# pulser enable
trbcmd setbit 0xc001 0xa101 0x2

#reset trigger logic
#trbcmd w 0xffff 0x20 0x33

#trbcmd setbit 0xc001 0xa101 0x8 # enable external trigger in of CTS
#trbcmd setbit 0xc001 0xa101 0x200 # enable trigger on TDCs hits

#
#echo "renumber the TDC channels"
#trbcmd s 0x86000008129ae828 0x00 0x0100
#trbcmd s 0x7b00000812432228 0x01 0x0101
#trbcmd s 0x80000008129add28 0x02 0x0102
#trbcmd s 0x61000008129b9e28 0x03 0x0103




